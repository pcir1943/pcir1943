package note.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import note.model.Corigent;
import note.model.Elev;
import note.model.Medie;
import note.model.Nota;

import note.repository.ClasaRepository;
import note.repository.ClasaRepositoryMock;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import note.utils.ClasaException;
import note.utils.Constants;

import note.controller.NoteController;

public class WBT_Test {
    private ClasaRepository clasa;
    private NoteController ctrl;

    @Before
    public void setUp() throws Exception {
        clasa = new ClasaRepositoryMock();
        ctrl = new NoteController();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void TC01_WBT() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.emptyRepository);
        clasa.calculeazaMedii();
    }

    @Test
    public void TC03_WBT() throws ClasaException {
//        expectedEx.expect(ClasaException.class);
//        expectedEx.expectMessage(Constants.emptyRepository);
        List<Elev> elevi = new ArrayList<Elev>();
        List<Nota> note = new ArrayList<Nota>();

        Elev e1 = new Elev(1,"marcel");
        elevi.add(e1);
        clasa.creazaClasa(elevi,note);

        List<Medie> medii = clasa.calculeazaMedii();
        assertEquals(medii.size(),1);
        assertEquals(medii.get(0).getMedie(),0,0.0001);

    }
    @Test
    public void TC07_WBT() throws ClasaException {
//        expectedEx.expect(ClasaException.class);
//        expectedEx.expectMessage(Constants.emptyRepository);
        List<Elev> elevi = new ArrayList<Elev>();
        List<Nota> note = new ArrayList<Nota>();

        Elev e1 = new Elev(1,"marcel");
        elevi.add(e1);
        Nota n1 = new Nota(1,"VVSS",10);
        note.add(n1);
        clasa.creazaClasa(elevi,note);

        List<Medie> medii = clasa.calculeazaMedii();
        assertEquals(medii.size(),1);
        assertEquals(medii.get(0).getMedie(),10,0.0001);

    }

}
