package note.test;

import static org.junit.Assert.*;

import java.util.List;

import note.model.Corigent;
import note.model.Elev;
import note.model.Medie;
import note.model.Nota;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import note.utils.ClasaException;
import note.utils.Constants;

import note.controller.NoteController;

public class MyTeste {

    NoteController ctrl;

    @Before
    public void setUp() throws Exception {
        ctrl = new NoteController();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void TC01_EC() throws ClasaException {
        Nota nota = new Nota(10, "Romana", 10);
        ctrl.addNota(nota);
        assertEquals(ctrl.getNote().size(),1);
    }

    @Test
    public void TC02_EC() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidMateria);
        Nota nota = new Nota(10, "Eng", 10);
        ctrl.addNota(nota);

        assertEquals(ctrl.getNote().size(),0);
    }

    @Test
    public void TC03_EC() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNota);
        Nota nota = new Nota(10, "Romana", 5.2);
        ctrl.addNota(nota);

        assertEquals(ctrl.getNote().size(),0);
    }


    @Test
    public void TC04_EC() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNrmatricol);
        Nota nota = new Nota(-10, "Romana", 10);
        ctrl.addNota(nota);

        assertEquals(ctrl.getNote().size(),0);
    }

    @Test
    public void TC05_EC() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNota);
        Nota nota = new Nota(10, "Romana", -11);
        ctrl.addNota(nota);

        assertEquals(ctrl.getNote().size(),0);
    }

    @Test
    public void TC01_BV() throws ClasaException {
        Nota nota = new Nota(10, "Roman", 10);
        ctrl.addNota(nota);
        assertEquals(ctrl.getNote().size(),1);
    }

    @Test
    public void TC02_BV() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidMateria);
        Nota nota = new Nota(10, "Roma", 10);
        ctrl.addNota(nota);

        assertEquals(ctrl.getNote().size(),0);
    }

    @Test
    public void TC03_BV() throws ClasaException {
        Nota nota = new Nota(10, "Romana", 5);
        ctrl.addNota(nota);
        assertEquals(ctrl.getNote().size(),1);
    }

    @Test
    public void TC04_BV() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNota);
        Nota nota = new Nota(10, "Romana", 5.0001);
        ctrl.addNota(nota);

        assertEquals(ctrl.getNote().size(),0);

    }
    @Test
    public void TC05_BV() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNrmatricol);
        Nota nota = new Nota(0, "Romana", 10);
        ctrl.addNota(nota);

        assertEquals(ctrl.getNote().size(),0);
    }

    @Test
    public void TC06_BV() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNrmatricol);
        Nota nota = new Nota(1001, "Romana", 10);
        ctrl.addNota(nota);

        assertEquals(ctrl.getNote().size(),0);

    }
}
